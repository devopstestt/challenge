FROM node:12 as builder 

WORKDIR /app

RUN npm install

RUN npm install express

COPY . .


FROM node:12-slim

WORKDIR /app

COPY --from=builder /app .

EXPOSE 11130

ENTRYPOINT ["node", "app.js"]

