const express = require('express')
const app = express()
const port = 11130

app.get('/', (req, res) => {
  res.send('Hello Hepsiburada from Ayşenur')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

